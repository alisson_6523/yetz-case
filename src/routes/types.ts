export interface routesProps {
    path: string;
    exact?: boolean;
    component: React.ComponentType<any>;
    isPrivate: boolean;
}
