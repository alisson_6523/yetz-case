import { PageLogin } from "../pages/Login";
import { Usuario } from "../pages/Usuario";
import { Novo } from "../pages/NovoUsuario";

import { routesProps } from "./types";

export const routes: routesProps[] = [
    {
        path: "/sistema/usuarios/novo",
        component: Novo,
        isPrivate: false,
    },
    {
        path: "/sistema/usuarios",
        component: Usuario,
        isPrivate: false,
    },

    { path: "/", exact: true, component: PageLogin, isPrivate: false },
];
