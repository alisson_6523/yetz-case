import { createGlobalStyle } from "styled-components";
import emailImg from "../assets/sistema/email.svg";
import cadeadoImg from "../assets/sistema/cadeado.svg";

export const GlobalStyle = createGlobalStyle`
    

    *{
        margin: 0px;
        padding: 0px;
        outline: 0;
        box-sizing: border-box;
    }

    #root{
        overflow-x: hidden;
    }

    .content{
        background: ${({ theme }) => theme.colors.fundoBase};
        position: absolute;
        top: 0px;
        left: 228px;
        width: calc(100% - 228px);
        height: 100vh;
        overflow: scroll;
        padding-top: 2.313rem;
        padding-left: 2.188rem;
        padding-right: 2.938rem;
        &::-webkit-scrollbar {
            -webkit-appearance: none;
        }

        &::-webkit-scrollbar:vertical {
            width: 5px;
        }

        &::-webkit-scrollbar:horizontal {
            height: 12px;
        }

        &::-webkit-scrollbar-thumb {
            background-color: rgba(89, 92, 118, 0.5);
        }

        &::-webkit-scrollbar-track {
            background-color: rgba(255, 255, 255, 0.1);
        }
    }

    .container{
        width: 1494px;
        margin: 0 auto;
    }


    html{
        @media(max-width: 1080px){
            font-size: 93.75%;
        }

        @media(max-width: 720px){
            font-size: 87.5%;
        }
    }

    body{
        -webkit-font-smoothing: antialiased;
    }

    body, input, textarea, button{
        font-family: 'Inter', sans-serif;
        font-weight: 400;
    }
    
    h1, h2, h3, h4, h5, h6, strong{
        font-weight: 600;
    }

    span{
        font-style: normal;
        font-weight: normal;
        font-size: 16px;
        line-height: 140%;
        color: #FFFFFF;
    }

    a{
        text-decoration: none;
    }

    button{
        cursor: pointer;
    }

    [disabled]{
        opacity: 0.6;
        cursor: not-allowed;
    }

    .table {
        .header{
            border-bottom: none !important;
            .th{
                font-weight: bold;    
                color: ${({ theme }) => theme.colors.gray700};
                border-bottom: 1px solid ${({ theme }) => theme.colors.line};
                padding-bottom: 16px;
                text-transform: uppercase;
            }
        }

        .tr {
            font-weight: 500;
            background-color: ${({ theme }) => theme.colors.black};
            &.header{
                padding-top: 44px;
                padding-bottom: 24px;
            }
            &.body {
                margin-bottom: 8px;
                height: 82px;
                padding-left: 24px;
                padding-right: 24px;
            }
        }
    }

    .container-input{
        width: 100%;
        margin-bottom: 16px;
        position: relative;
        &:last-child{
            margin-bottom: 0px;
        }
        
        input{
            width: 100%;
            height: 56px;
            border: 1px solid rgba(92, 100, 113, 0.9);
            border-radius: 4px;
            padding-left: 40px;

            font-style: normal;
            font-weight: normal;
            font-size: 16px;
            line-height: 150%;
            letter-spacing: -0.005em;
            color: #FFFFFF;
            opacity: 0.75;

            background-color: transparent;
        }
        &.email{
            &:before{
                content: "";
                display: block;
                width: 21px;
                height: 18px;
                position: absolute;
                top: 50%;
                left: 21px;
                transform: translate(-50%, -50%);
                -webkit-mask: url(${emailImg});
                -webkit-mask-repeat: no-repeat;
                background: #ffffff80;
            }
        }
        &.senha{
            &:before{
                content: "";
                display: block;
                width: 21px;
                height: 21px;
                position: absolute;
                top: 50%;
                left: 21px;
                transform: translate(-50%, -50%);
                -webkit-mask: url(${cadeadoImg});
                -webkit-mask-repeat: no-repeat;
                background: #ffffff80;
            }
        }
    }

    .combo-box-filtro{
        &.MuiAutocomplete-root{
            .MuiAutocomplete-input{
                color: ${({ theme }) => theme.colors.white};
            }

            .MuiAutocomplete-popper{
                background: linear-gradient(90deg, #19FFFF 1.92%, rgba(25, 255, 255, 0) 105.77%);
            }

            .MuiInputLabel-root{
                &.MuiInputLabel-formControl{
                    color: ${({ theme }) => theme.colors.action};
                    font-weight: bold;
                    font-size: 12px;
                }
            }
            .MuiOutlinedInput-notchedOutline{
                border: none;
            }
        }
    }

    .combo-box-select{
        &.MuiAutocomplete-root{

            &:hover{
                .MuiOutlinedInput-notchedOutline{
                    border: 1px solid #13393B;
                }
            }
            .MuiAutocomplete-input{
                color: ${({ theme }) => theme.colors.white};
            }

            .MuiAutocomplete-popper{
                background: linear-gradient(90deg, #19FFFF 1.92%, rgba(25, 255, 255, 0) 105.77%);
            }

            .MuiInputLabel-root{
                &.MuiInputLabel-formControl{
                    color: ${({ theme }) => theme.colors.action};
                    font-weight: bold;
                    font-size: 12px;
                }
            }
            .MuiAutocomplete-endAdornment{
                top: calc(50% - 7px);
                right: 20px !important;
            }
            .MuiOutlinedInput-notchedOutline{
                border: 1px solid #13393B !important;
            }
        }
    }
`;
