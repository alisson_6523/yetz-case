import * as Yup from "yup";
import { Option } from "../../components/Select/types";

export interface MyFormValues {
    nome: string;
    login: string;
    senha: string;
    perfil: string;
}

export interface SegmentacaoType {
    campanha: Option;
    cartao: Option;
    nivel: Option;
}

export const initialValues: MyFormValues = {
    login: "",
    nome: "",
    perfil: "",
    senha: "",
};

export const SegmentacaoInitialValues: SegmentacaoType = {
    campanha: { label: "", value: 0 },
    cartao: { label: "", value: 0 },
    nivel: { label: "", value: 0 },
};

export const validadtion = Yup.object().shape({
    login: Yup.string().required("Campo Obrigatório"),
    nome: Yup.string().required("Campo Obrigatório"),
    perfil: Yup.string().required("Campo Obrigatório"),
    senha: Yup.string().required("Campo Obrigatório"),
});

export const validadtionSegmentacao = Yup.object().shape({
    campanha: Yup.object().shape({
        label: Yup.string().required("Campo Obrigatório"),
        value: Yup.number().required("Campo Obrigatório"),
    }),
    cartao: Yup.object().shape({
        label: Yup.string().required("Campo Obrigatório"),
        value: Yup.number().required("Campo Obrigatório"),
    }),
    nivel: Yup.object().shape({
        label: Yup.string().required("Campo Obrigatório"),
        value: Yup.number().required("Campo Obrigatório"),
    }),
});
