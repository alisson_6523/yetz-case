import { Text, Flex, Box } from "aplication-yetz";
import { CardStyles } from "./styles";

export function Card() {
    return (
        <CardStyles>
            <Flex>
                <Box>
                    <Text
                        fontSize="mdl"
                        fontWeight="600"
                        color="white"
                        mb="12px"
                    >
                        Atento
                    </Text>

                    <Text fontSize="md" color="gray700" mb="8px">
                        Cartão
                    </Text>
                    <Text fontSize="md" color="gray700">
                        1º Nível Voz
                    </Text>
                </Box>
            </Flex>
        </CardStyles>
    );
}
