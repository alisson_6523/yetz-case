import { useUser } from "../../contexts/UserContext";
import { Text, Flex, Box } from "aplication-yetz";
import { Formik, Form, FormikProps } from "formik";
import {
    SegmentacaoInitialValues,
    validadtionSegmentacao,
    SegmentacaoType,
} from "./type";

import { Button, Cancelar } from "../../components/Button";
import { Select } from "../../components/Select";
import { Option } from "../../components/Select/types";
import closeImg from "../../assets/sistema/close-modal.svg";

import { Modal } from "./styles";

export function Segmentacao() {
    const { closeModal, addSegmentacao } = useUser();

    const options: Option[] = [
        {
            label: "Teste 1",
            value: 1,
        },
        {
            label: "Teste 2",
            value: 2,
        },
        {
            label: "Teste 3",
            value: 3,
        },
    ];
    const cartao: Option[] = [
        {
            label: "Cartão 1",
            value: 1,
        },
        {
            label: "Cartão 2",
            value: 2,
        },
        {
            label: "Cartão 3",
            value: 3,
        },
    ];
    const nivel: Option[] = [
        {
            label: "Nivel 1",
            value: 1,
        },
        {
            label: "Nivel 2",
            value: 2,
        },
        {
            label: "Nivel 3",
            value: 3,
        },
    ];

    return (
        <Modal>
            <Formik
                initialValues={SegmentacaoInitialValues}
                validationSchema={validadtionSegmentacao}
                onSubmit={(values, actions) => {
                    addSegmentacao(values);
                    closeModal();
                }}
            >
                {(props: FormikProps<SegmentacaoType>) => {
                    const { setFieldValue, errors, touched } = props;

                    return (
                        <Form>
                            <Flex
                                alignItems="center"
                                justifyContent="space-between"
                                mb="39px"
                            >
                                <Text fontSize="mdl" color="white" bold={true}>
                                    Adicionar Segmento
                                </Text>

                                <button
                                    type="button"
                                    className="close"
                                    onClick={() => closeModal()}
                                >
                                    <img src={closeImg} alt="" />
                                </button>
                            </Flex>

                            <Flex mb="16px" flexDirection="column">
                                <Select
                                    placeholder="Atento"
                                    options={options}
                                    onChange={(value) => {
                                        console.log(value);
                                        setFieldValue("campanha", value);
                                    }}
                                />
                                <>
                                    {errors.campanha?.label &&
                                    touched.campanha?.label ? (
                                        <span>{errors.campanha.label}</span>
                                    ) : null}
                                </>
                            </Flex>

                            <Flex mb="16px" flexDirection="column">
                                <Select
                                    placeholder="Cartão"
                                    options={cartao}
                                    onChange={(value) =>
                                        setFieldValue("cartao", value)
                                    }
                                />
                                <>
                                    {errors.cartao?.label &&
                                    touched.cartao?.label ? (
                                        <span>{errors.cartao.label}</span>
                                    ) : null}
                                </>
                            </Flex>

                            <Flex mb="39px" flexDirection="column">
                                <Select
                                    placeholder="Nível Voz"
                                    options={nivel}
                                    onChange={(value) =>
                                        setFieldValue("nivel", value)
                                    }
                                />
                                <>
                                    {errors.nivel?.label &&
                                    touched.cartao?.label ? (
                                        <span> {errors.nivel.label} </span>
                                    ) : null}
                                </>
                            </Flex>

                            <Flex>
                                <Box width="217px">
                                    <Button
                                        title="Adicionar SEGMENTO"
                                        upperCase={true}
                                    />
                                </Box>
                                <Box width="136px" ml="24px">
                                    <Cancelar
                                        title="cancelar"
                                        upperCase={true}
                                        onClick={() => closeModal()}
                                    />
                                </Box>
                            </Flex>
                        </Form>
                    );
                }}
            </Formik>
        </Modal>
    );
}
