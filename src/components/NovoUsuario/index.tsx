import { useUser } from "../../contexts/UserContext";
import { LayoutEntityAdd } from "../LayoutEntityAdd";
import { FormUsuario } from "./form";
import { Modal } from "../Modal";
import { Segmentacao } from "./segmenatacao";

export function NovoUsuario() {
    const { modal, openModal, segmentacaoArr } = useUser();

    function handleModalOpen() {
        openModal();
    }

    return (
        <>
            <LayoutEntityAdd title="Novo usuario">
                <FormUsuario
                    openModal={handleModalOpen}
                    segmentacao={segmentacaoArr}
                />
            </LayoutEntityAdd>

            <Modal component={Segmentacao} active={modal} />
        </>
    );
}
