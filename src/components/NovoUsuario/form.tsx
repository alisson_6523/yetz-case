import { Box } from "aplication-yetz";
import { Formik, Form, Field, ErrorMessage } from "formik";

import { initialValues, validadtion, SegmentacaoType } from "./type";

import { Button } from "../../components/Button";
import { Card } from "./card";

import buttonImg from "../../assets/sistema/segmentacao.svg";

import { Segmentacao } from "./styles";

interface FomUsuarioProps {
    openModal: () => void;
    segmentacao: SegmentacaoType[];
}

export function FormUsuario(props: FomUsuarioProps) {
    const { openModal, segmentacao } = props;

    return (
        <Box width={520}>
            <Formik
                initialValues={initialValues}
                validationSchema={validadtion}
                onSubmit={(values, actions) => {
                    console.log({ values, actions });
                }}
            >
                <Form>
                    <div className="container-input">
                        <Field type="text" name="nome" placeholder="Nome" />
                        <ErrorMessage component="span" name="nome" />
                    </div>
                    <div className="container-input">
                        <Field type="text" name="login" placeholder="Login" />
                        <ErrorMessage component="span" name="login" />
                    </div>
                    <div className="container-input">
                        <Field type="text" name="senha" placeholder="Senha" />
                        <ErrorMessage component="span" name="senha" />
                    </div>
                    <div className="container-input">
                        <Field type="text" name="perfil" placeholder="Perfil" />
                        <ErrorMessage component="span" name="perfil" />
                    </div>

                    <Segmentacao>
                        <h3>Segmentação</h3>

                        <button type="button" onClick={openModal}>
                            <img src={buttonImg} alt="" />
                        </button>
                    </Segmentacao>

                    <Card />

                    <Box width={202} mt={32}>
                        <Button title="Adicionar USUÁRIO" upperCase={true} />
                    </Box>
                </Form>
            </Formik>
        </Box>
    );
}
