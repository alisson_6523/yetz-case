export interface ButtonProps {
    title: string;
    onClick?: () => void;
    upperCase?: boolean;
}
