import { Container, Cancelar as CancelarStyles } from "./styles";
import { ButtonProps } from "./types";

export function Button(props: ButtonProps) {
    const { title, onClick, upperCase = false } = props;

    return (
        <Container upperCase={upperCase} onClick={onClick} type="submit">
            {title}
        </Container>
    );
}

export function Cancelar(props: ButtonProps) {
    const { title, onClick, upperCase = false } = props;

    return (
        <CancelarStyles upperCase={upperCase} onClick={onClick} type="button">
            {title}
        </CancelarStyles>
    );
}
