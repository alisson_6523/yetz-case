export interface Option {
    label: string;
    value: number;
}
export interface SelectProps {
    placeholder: string;
    options: Option[];
    onChange: (value: Option) => void;
}
