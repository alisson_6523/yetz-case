import React from "react";

export interface LayoutEntityAddProps {
    title: string;
    children: React.ReactChild | React.ReactChild[];
}
