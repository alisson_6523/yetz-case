import styled from "styled-components";

export const Container = styled.div`
    display: grid;
    grid-template-columns: 717px 1fr;
    .esq {
        background: ${({ theme }) => theme.colors.primary};
        width: 100%;
        height: 100vh;
        padding-top: 2.75rem;
        padding-right: 4.563rem;
        img {
            margin-left: auto;
            display: block;
        }
    }

    .dir {
        padding-top: 4.25rem;
        padding-left: 5.75rem;
        background: ${({ theme }) => theme.colors.fundoBase};

        .header {
            display: flex;
            align-items: center;
            border-bottom: 1px solid rgba(255, 255, 255, 0.1);
            padding-bottom: 32px;
            margin-bottom: 40px;
        }
    }
`;
