import styled from "styled-components";

export const Container = styled.div`
    background: ${({ theme }) => theme.colors.fundoBase};
    border-radius: 16px;
    min-height: 239px;

    text-align: center;

    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;

    padding: 0 80px;

    p {
        width: 172px;
    }

    img {
        margin-bottom: 16px;
    }
`;
