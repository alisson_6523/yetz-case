import { Text } from "aplication-yetz";
import cadeadoImg from "../../assets/sistema/cadeado-vermelh.svg";

import { Container } from "./styles";

export function BoxNotificacao() {
    return (
        <Container>
            <img src={cadeadoImg} alt="" />

            <Text as="p" fontWeight="600" fontSize="mdl" color="white">
                Dados incorretos, tente novamente
            </Text>
        </Container>
    );
}
