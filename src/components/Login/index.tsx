import { MouseEvent } from "react";
import { Modal } from "../Modal";
import { FormLogin } from "./FormLogin";
import { BoxNotificacao } from "../BoxNotificacao";
import { Container, LogoContainer } from "./styles";

import logoImg from "../../assets/sistema/logo-login.svg";

export function Login() {
    function handleClick(e: MouseEvent) {
        console.log(e.target);
    }

    return (
        <>
            <Container>
                <FormLogin />

                <LogoContainer>
                    <img src={logoImg} alt="" />
                </LogoContainer>
            </Container>

            <Modal
                onClick={handleClick}
                component={BoxNotificacao}
                active={false}
            />
        </>
    );
}
