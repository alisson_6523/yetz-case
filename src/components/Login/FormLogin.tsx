import { Box, Heading } from "aplication-yetz";
import { Button } from "../Button";
import { Formstyles } from "./styles";
export function FormLogin() {
    function handleClick() {
        console.log("olá");
    }
    return (
        <Formstyles>
            <Box width="381px" ml="auto" mr="139px">
                <Heading as="h2" color="white" mb="24px">
                    Acesse sua área
                </Heading>
                <div className="container-input email">
                    <input type="text" placeholder="E-mail Corporativo" />
                </div>
                <div className="container-input senha">
                    <input type="text" placeholder="Senha" />
                </div>

                <Button title="Entrar" onClick={handleClick} />
            </Box>
        </Formstyles>
    );
}
