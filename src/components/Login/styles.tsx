import styled from "styled-components";

export const Container = styled.div`
    width: 100vw;
    height: 100vh;
    background: ${({ theme }) => theme.colors.primary};
    display: grid;
    grid-template-columns: 872px 1fr;
`;

export const Formstyles = styled.div`
    width: 100%;
    height: 100%;
    padding-top: 12.688rem;
    margin-left: auto;
    margin-right: 140px;
    background: ${({ theme }) => theme.colors.fundoBase};
`;

export const LogoContainer = styled.div`
    width: 100%;
    background: ${({ theme }) => theme.colors.primary};
    padding-top: 13.75rem;
    margin-left: 141px;
`;
