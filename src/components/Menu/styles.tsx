import styled from "styled-components";

export const Container = styled.div`
    width: 228px;
    height: 100vh;
    background: ${({ theme }) => theme.colors.primary};
    display: flex;
    align-items: flex-start;
    justify-content: center;
    padding-top: 3.125rem;
`;
