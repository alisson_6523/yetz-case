import { Container } from "./styles";
import imgLogo from "../../assets/sistema/lolgo-menu.svg";
export function Menu() {
    return (
        <Container>
            <img src={imgLogo} alt="" />
        </Container>
    );
}
