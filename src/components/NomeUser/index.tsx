import { Flex, Text } from "aplication-yetz";
import userImg from "../../assets/sistema/user-sistem.svg";

export function NomeUser() {
    return (
        <Flex alignItems="center">
            <img src={userImg} alt="" />
            <Text ml="16px" fontFamily="Roboto" color="white">
                Olá Mario José Hernandes Franco
            </Text>
        </Flex>
    );
}
