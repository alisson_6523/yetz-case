import React, { MouseEvent } from "react";

export interface ModalProps {
    component: React.FunctionComponent<any>;
    active: boolean;

    onClick?: (e: MouseEvent<Element>) => void;
}
