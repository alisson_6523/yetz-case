import { ModalProps } from "./types";
import { Container } from "./styles";

export function Modal(props: ModalProps) {
    const { component: Component, active, onClick } = props;

    // function closeModal(e: MouseEvent) {
    //     const element = (e.target as HTMLElement).dataset;
    //     if (element?.closemodal) {
    //     }
    // }

    return (
        <Container onClick={onClick} active={active} data-closemodal="close">
            <div className="container-body">
                <Component />
            </div>
        </Container>
    );
}
