import { BrowserRouter } from "react-router-dom";
import { routes } from "./routes";
import { SwitchRoutes } from "./components/SwitchRoutes";

import { GlobalStyle } from "./styles/global";

export const teste = () => {
    return <h1>olá</h1>;
};

export function App() {
    return (
        <>
            <GlobalStyle />
            <BrowserRouter>
                <SwitchRoutes routes={routes} />
            </BrowserRouter>
        </>
    );
}
