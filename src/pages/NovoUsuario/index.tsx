import { UserProvider } from "../../contexts/UserContext";
import { NovoUsuario } from "../../components/NovoUsuario";

export function Novo() {
    return (
        <UserProvider>
            <NovoUsuario />
        </UserProvider>
    );
}
