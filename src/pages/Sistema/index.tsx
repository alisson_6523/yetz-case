import { Box, Flex, Text, Heading, Grid, BoxSimple } from "aplication-yetz";

import logoImg from "../../assets/sistema/logo-vinho.svg";
import setaDirImg from "../../assets/sistema/seta-dir.svg";

import { config } from "./config";

export function Sistema() {
    return (
        <div className="container">
            <Box>
                <Flex
                    alignItems="center"
                    justifyContent="space-between"
                    pt="4.75rem"
                    pb="8.438rem"
                >
                    <img src={logoImg} alt="" />

                    <Box>
                        <Text color="primary" mr="22px">
                            Sair
                        </Text>
                        <img src={setaDirImg} alt="" />
                    </Box>
                </Flex>

                <Flex>
                    <Box mb="4.375rem">
                        <Text color="black" pb="2rem">
                            Olá André
                        </Text>

                        <Heading as="h2">Como deseja proseguir?</Heading>
                    </Box>
                </Flex>

                <Grid gridTemplateColumns="repeat(6,1fr)">
                    {config.map((itens) => (
                        <BoxSimple color="gray800" {...itens} />
                    ))}
                </Grid>
            </Box>
        </div>
    );
}
