import produtoImg from "../../assets/sistema/produto.svg";
import estoqueImg from "../../assets/sistema/estoque.svg";
import pedidoImg from "../../assets/sistema/pedido.svg";
import plataformaImg from "../../assets/sistema/plataforma.svg";
import relatorioImg from "../../assets/sistema/relatorio.svg";
import usuarioImg from "../../assets/sistema/usuario.svg";

export interface box {
    name: string;
    img: string;
    link: string;
}

export const config: box[] = [
    {
        name: "Produto",
        img: produtoImg,
        link: "/",
    },
    {
        name: "Estoque",
        img: estoqueImg,
        link: "/",
    },
    {
        name: "Pedido",
        img: pedidoImg,
        link: "/",
    },
    {
        name: "Plataformas",
        img: plataformaImg,
        link: "/",
    },
    {
        name: "Relatórios",
        img: relatorioImg,
        link: "/",
    },
    {
        name: "Usuários",
        img: usuarioImg,
        link: "/",
    },
];
