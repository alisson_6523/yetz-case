import styled from "styled-components";

export const ContainerHeader = styled.div`
    padding-bottom: 24px;
    border-bottom: 1px solid #ffffff1a;
`;

export const BoxFilter = styled.div`
    input {
        background: transparent;
        color: ${({ theme }) => theme.colors.white};
    }
`;
