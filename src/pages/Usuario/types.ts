import React from "react";

export interface Data {
    nome: string | React.ReactChild | React.ReactChild[] | React.ReactNode;
    login: string | React.ReactChild | React.ReactChild[] | React.ReactNode;
    perfil: string | React.ReactChild | React.ReactChild[] | React.ReactNode;
    case: string | React.ReactChild | React.ReactChild[] | React.ReactNode;
    acoes: string | React.ReactChild | React.ReactChild[] | React.ReactNode;
}
