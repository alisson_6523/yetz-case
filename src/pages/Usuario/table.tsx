import { Table, Text, Box } from "aplication-yetz";
import { NomeUser } from "../../components/NomeUser";
import { column } from "./config";
import { Data } from "./types";

import editarImg from "../../assets/sistema/editar.svg";

export function Tabela() {
    const data: Data[] = [
        {
            nome: <NomeUser />,
            login: (
                <Text color="white">mariojosehernandes@yetzcards.com.br</Text>
            ),
            perfil: <Text color="white">Avaliador</Text>,
            case: <Text color="white">2/20</Text>,
            acoes: (
                <Box ml="auto" width="24px">
                    <img src={editarImg} alt="" />
                </Box>
            ),
        },
        {
            nome: <NomeUser />,
            login: (
                <Text color="white">mariojosehernandes@yetzcards.com.br</Text>
            ),
            perfil: <Text color="white">Avaliador</Text>,
            case: <Text color="white">2/20</Text>,
            acoes: (
                <Box ml="auto" width="24px">
                    <img src={editarImg} alt="" />
                </Box>
            ),
        },
        {
            nome: <NomeUser />,
            login: (
                <Text color="white">mariojosehernandes@yetzcards.com.br</Text>
            ),
            perfil: <Text color="white">Avaliador</Text>,
            case: <Text color="white">2/20</Text>,
            acoes: (
                <Box ml="auto" width="24px">
                    <img src={editarImg} alt="" />
                </Box>
            ),
        },
        {
            nome: <NomeUser />,
            login: (
                <Text color="white">mariojosehernandes@yetzcards.com.br</Text>
            ),
            perfil: <Text color="white">Avaliador</Text>,
            case: <Text color="white">2/20</Text>,
            acoes: (
                <Box ml="auto" width="24px">
                    <img src={editarImg} alt="" />
                </Box>
            ),
        },
        {
            nome: <NomeUser />,
            login: (
                <Text color="white">mariojosehernandes@yetzcards.com.br</Text>
            ),
            perfil: <Text color="white">Avaliador</Text>,
            case: <Text color="white">2/20</Text>,
            acoes: (
                <Box ml="auto" width="24px">
                    <img src={editarImg} alt="" />
                </Box>
            ),
        },
        {
            nome: <NomeUser />,
            login: (
                <Text color="white">mariojosehernandes@yetzcards.com.br</Text>
            ),
            perfil: <Text color="white">Avaliador</Text>,
            case: <Text color="white">2/20</Text>,
            acoes: (
                <Box ml="auto" width="24px">
                    <img src={editarImg} alt="" />
                </Box>
            ),
        },
        {
            nome: <NomeUser />,
            login: (
                <Text color="white">mariojosehernandes@yetzcards.com.br</Text>
            ),
            perfil: <Text color="white">Avaliador</Text>,
            case: <Text color="white">2/20</Text>,
            acoes: (
                <Box ml="auto" width="24px">
                    <img src={editarImg} alt="" />
                </Box>
            ),
        },
    ];

    return <Table columns={column} data={data} />;
}
