import { Flex, Heading } from "aplication-yetz";
import { Filtro } from "../../components/Select";

export function SubHeader() {
    return (
        <>
            <Flex pt="3.625rem" justifyContent="space-between">
                <Heading as="h2" color="white">
                    Todos Usuários
                </Heading>

                <Filtro />
            </Flex>
        </>
    );
}
