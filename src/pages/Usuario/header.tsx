import { Flex, Filter, Box } from "aplication-yetz";
import { Button } from "../../components/Button";
import { NomeUser } from "../../components/NomeUser";

import { BoxFilter, ContainerHeader } from "./styles";

export function Header() {
    return (
        <ContainerHeader>
            <Flex justifyContent="space-between">
                <NomeUser />

                <BoxFilter>
                    <Filter
                        iconFilter={false}
                        color="action"
                        borderColor="action"
                    />
                </BoxFilter>
                <Box width="202px">
                    <Button title="Adicionar USUÁRIO" />
                </Box>
            </Flex>
        </ContainerHeader>
    );
}
