import { Menu } from "../../components/Menu";
import { Header } from "./header";
import { SubHeader } from "./subHeader";
import { Tabela } from "./table";

export function Usuario() {
    return (
        <>
            <Menu />

            <div className="content">
                <Header />
                <SubHeader />
                <Tabela />
            </div>
        </>
    );
}
