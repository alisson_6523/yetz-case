import React, { createContext, useState, useContext } from "react";
import { SegmentacaoType } from "../components/NovoUsuario/type";

interface UserProviderProps {
    children: React.ReactChild | React.ReactChild[] | React.ReactNode;
}

interface UserContextData {
    modal: boolean;
    segmentacaoArr: SegmentacaoType[];
    openModal: () => void;
    closeModal: () => void;
    addSegmentacao: (values: SegmentacaoType) => void;
}

const UserContext = createContext<UserContextData>({} as UserContextData);

export function UserProvider(props: UserProviderProps) {
    const [modal, setModal] = useState<boolean>(false);
    const [segmentacaoArr, setSegmentacaoArr] = useState<SegmentacaoType[]>([]);

    function openModal() {
        setModal(true);
    }

    function closeModal() {
        setModal(false);
    }

    function addSegmentacao(values: SegmentacaoType) {
        setSegmentacaoArr([...segmentacaoArr, values]);
    }

    const { children } = props;

    return (
        <UserContext.Provider
            value={{
                modal,
                openModal,
                closeModal,
                addSegmentacao,
                segmentacaoArr,
            }}
        >
            {children}
        </UserContext.Provider>
    );
}

export function useUser() {
    const context = useContext(UserContext);
    return context;
}
